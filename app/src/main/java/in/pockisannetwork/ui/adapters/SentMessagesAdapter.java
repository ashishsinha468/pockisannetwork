package in.pockisannetwork.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import in.pockisannetwork.R;
import in.pockisannetwork.ui.models.Message;
import in.pockisannetwork.utils.DateUtils;

/**
 * Created by ashish on 19/4/17.
 */

public class SentMessagesAdapter extends RecyclerView.Adapter<SentMessagesAdapter.ViewHolder> {
    List<Message> messages = Collections.emptyList();
    LayoutInflater layoutInflater;
    private Context context;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.row_item_messages, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Message message = messages.get(position);
        holder.tvName.setText(message.getContactName());
        holder.tvOtp.setText(message.getOtp());
        String dateAndTimeFromTimestampString = DateUtils.getDateAndTimeFromTimestampString(message.getTimestamp());
        holder.tvSentAt.setText(dateAndTimeFromTimestampString);
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @InjectView(R.id.tv_name)
        TextView tvName;

        @InjectView(R.id.tv_otp)
        TextView tvOtp;

        @InjectView(R.id.tv_sent_at)
        TextView tvSentAt;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
        }
    }
}
