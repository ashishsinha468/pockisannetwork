package in.pockisannetwork.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import in.pockisannetwork.R;
import in.pockisannetwork.communication.model.domain.Contact;
import in.pockisannetwork.ui.interfaces.ContactsCallback;

import java.util.Collections;
import java.util.List;

/**
 * Created by ashish on 18/4/17.
 */

public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ViewHolder> {
    List<Contact> contacts = Collections.emptyList();
    Context context;
    LayoutInflater layoutInflater;
    private ContactsCallback callback;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.row_item_contacts, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Contact contact = contacts.get(position);
        String firstName = contact.getFirstName();
        String lastName = contact.getLastName();
        holder.tvContactName.setText(firstName + " " + lastName);
        holder.llContainerContactName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onContactNameClicked(contact);
            }
        });
    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }


    public void setCallback(ContactsCallback callback) {
        this.callback = callback;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @InjectView(R.id.ll_container_contact_name)
        LinearLayout llContainerContactName;

        @InjectView(R.id.tv_contact_name)
        TextView tvContactName;//Appending first and last name to show contact name..assuming first name and last name are individually not important as of now

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
        }
    }
}
