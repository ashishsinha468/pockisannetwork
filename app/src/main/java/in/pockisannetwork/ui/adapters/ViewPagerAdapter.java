package in.pockisannetwork.ui.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

import in.pockisannetwork.ui.models.ViewPagerTab;

/**
 * Created by ashish on 18/4/17.
 */

public class ViewPagerAdapter extends FragmentPagerAdapter {
    List<ViewPagerTab> viewPagerTabs;

    public ViewPagerAdapter(FragmentManager supportFragmentManager) {
        super(supportFragmentManager);
        viewPagerTabs = new ArrayList<>();
    }

    @Override
    public Fragment getItem(int position) {
        ViewPagerTab viewPagerTab = viewPagerTabs.get(position);
        Fragment fragment = viewPagerTab.getFragment();
        return fragment;
    }

    @Override
    public int getCount() {
        return viewPagerTabs.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return viewPagerTabs.get(position).getTitle();
    }

    public void addTab(ViewPagerTab viewPagerTab) {
        viewPagerTabs.add(viewPagerTab);
    }
}
