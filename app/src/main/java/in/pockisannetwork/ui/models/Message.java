package in.pockisannetwork.ui.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ashish on 19/4/17.
 */

public class Message {

    @SerializedName("text")
    private String text;
    @SerializedName("otp")
    private String otp;

    @SerializedName("contactName")
    private String contactName;

    @SerializedName("timestamp")
    private String timestamp;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return text + ": " + otp;
    }
}
