package in.pockisannetwork.ui.models;

import android.support.v4.app.Fragment;

/**
 * Created by ashish on 18/4/17.
 */

public class ViewPagerTab {
    Fragment fragment;
    String title;

    public ViewPagerTab(Fragment fragment, String title) {
        this.fragment = fragment;
        this.title = title;
    }

    public Fragment getFragment() {
        return fragment;
    }

    public void setFragment(Fragment fragment) {
        this.fragment = fragment;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}

