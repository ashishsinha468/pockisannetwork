package in.pockisannetwork.ui.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import in.pockisannetwork.R;
import in.pockisannetwork.application.Constants;
import in.pockisannetwork.communication.model.domain.Contact;
import in.pockisannetwork.communication.presenter.ContactInfoPresenter;
import in.pockisannetwork.communication.presenter.interfaces.IContactInfoView;
import in.pockisannetwork.ui.interfaces.SendMessageCallback;
import in.pockisannetwork.ui.models.Message;
import in.pockisannetwork.utils.DialogUtils;
import in.pockisannetwork.utils.GsonUtils;

/**
 * Created by ashish on 19/4/17.
 */

public class ContactInfoActivity extends BaseActivity implements IContactInfoView {
    @InjectView(R.id.toolbar)
    Toolbar toolbar;

    @InjectView(R.id.tv_contact_name)
    TextView tvContactName;

    @InjectView(R.id.tv_phone_number)
    TextView tvPhoneNumber;

    ProgressDialog progressDialog;

    private Contact contact;
    private ContactInfoPresenter presenter;

    public static Intent newIntent(Context context, Contact contact) {
        Intent intent = new Intent(context, ContactInfoActivity.class);
        intent.putExtra(Constants.BUNDLE_KEYS.CONTACT, GsonUtils.getJsonString(contact));
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_info);
        ButterKnife.inject(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.label_please_wait));
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter = new ContactInfoPresenter();
        init();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.detachView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return false;
            //no need for default
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    private void init() {
        initToolbar();
        initPresenter();
        initData();
        showContactName();
        showContactNumber();
    }

    private void initToolbar() {
        toolbar.setTitle(getString(R.string.app_name));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initPresenter() {
        presenter = new ContactInfoPresenter();
        if (presenter.getView() == null) {
            presenter.attachView(this);
        }
    }

    private void showContactNumber() {
        tvPhoneNumber.setText(contact.getPhoneNumber());
    }

    private void showContactName() {
        String firstName = contact.getFirstName();
        String lastName = contact.getLastName();
        String displayName = firstName + " " + lastName;
        tvContactName.setText(displayName);
    }

    private void initData() {
        String contactString = getIntent().getStringExtra(Constants.BUNDLE_KEYS.CONTACT);
        contact = GsonUtils.getObjectFromJson(contactString, Contact.class);
    }

    @OnClick(R.id.btn_send_message)
    public void onSendMessageButtonClicked() {
        DialogUtils.showSendMessageScreen(new SendMessageCallback() {
            @Override
            public void onYesButtonClicked(Message message) {
                if (isAValidPhoneNumber(contact.getPhoneNumber())) {
                    showProgressDialog();
                    presenter.sendMessageTo(contact, message);
                } else {
                    showMessage(getString(R.string.label_invalid_phone_number_cannot_send_message_to_this_contact), Snackbar.LENGTH_SHORT);
                }
            }
        }, this);
    }

    private boolean isAValidPhoneNumber(String phoneNumber) {
        if (TextUtils.isEmpty(phoneNumber) || phoneNumber.length() != 10 || !TextUtils.isDigitsOnly(phoneNumber)) {
            return false;
        }
        return true;
    }

    private void showProgressDialog() {
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showMessage(String message, int notificationInfo) {
        hideProgressDialog();
        snack(toolbar, message, notificationInfo, Snackbar.LENGTH_SHORT);
    }
}
