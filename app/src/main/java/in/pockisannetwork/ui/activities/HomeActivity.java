package in.pockisannetwork.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import butterknife.ButterKnife;
import butterknife.InjectView;
import in.pockisannetwork.R;
import in.pockisannetwork.application.Constants;
import in.pockisannetwork.communication.model.domain.Contact;
import in.pockisannetwork.ui.activities.BaseActivity;
import in.pockisannetwork.ui.activities.ContactInfoActivity;
import in.pockisannetwork.ui.adapters.ViewPagerAdapter;
import in.pockisannetwork.ui.fragments.ContactsFragment;
import in.pockisannetwork.ui.fragments.SentMessagesFragment;
import in.pockisannetwork.ui.models.ViewPagerTab;

public class HomeActivity extends BaseActivity {

    @InjectView(R.id.toolbar)
    Toolbar toolbar;

    @InjectView(R.id.tl_home)
    TabLayout tlHome;

    @InjectView(R.id.vp_home)
    ViewPager vpHome;
    private Fragment sentMessagesFragment;
    private Fragment contactsFragment;
    private ViewPagerAdapter vpAdapter;
    private boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //I generally use butter knife to inject views..saves boiler plate codes to find view by id every time
        ButterKnife.inject(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return false;
            //no need for default
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        toast(getString(R.string.doubleback_press_message));

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }


    private void init() {
        initToolbar();
        initViewPager();
        initTabLayout();
    }

    private void initTabLayout() {
        tlHome.setupWithViewPager(vpHome);
    }

    private void initViewPager() {
        vpAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        contactsFragment = ContactsFragment.getInstance();
        vpAdapter.addTab(new ViewPagerTab(contactsFragment, getString(R.string.label_tab_contacts)));

        sentMessagesFragment = SentMessagesFragment.getInstance();
        vpAdapter.addTab(new ViewPagerTab(sentMessagesFragment, getString(R.string.label_tab_sent_messages)));
        vpHome.setAdapter(vpAdapter);
    }

    private void initToolbar() {
        toolbar.setTitle(getString(R.string.app_name));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    public void showMessage(String message, int notificationInfo) {
        snack(toolbar, message, notificationInfo, Snackbar.LENGTH_SHORT);
    }

    public void onCustomerContactClicked(Contact contact) {
        Intent intent = ContactInfoActivity.newIntent(this, contact);
        startActivity(intent);
        //should have done starActivityForResult, but anyways i re fetch data on resume so no need over here
    }
}
