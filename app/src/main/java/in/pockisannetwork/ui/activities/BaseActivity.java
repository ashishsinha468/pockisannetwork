package in.pockisannetwork.ui.activities;

import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

/**
 * Created by ashish on 18/4/17.
 */

//This activity handles all my helper methoda, which other activities might use..all my other activities extend this  activity
public class BaseActivity extends AppCompatActivity {
    public void snack(View view, String message, int notificationType, int duration) {
        Snackbar snackbar = Snackbar.make(view, message, duration)
                .setAction("Action", null);
        snackbar.getView().setBackgroundColor(ContextCompat.getColor(this, notificationType));
        snackbar.show();
    }

    public void snackWithCallback(View view, String message, int notificationType, int duration, String actionText, View.OnClickListener callback) {
        Snackbar snackbar = Snackbar.make(view, message, duration)
                .setAction(actionText, callback);
        snackbar.getView().setBackgroundColor(ContextCompat.getColor(this, notificationType));
        snackbar.show();
    }

    public void toast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
