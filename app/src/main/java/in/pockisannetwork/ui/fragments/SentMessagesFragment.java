package in.pockisannetwork.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import in.pockisannetwork.ui.activities.HomeActivity;
import in.pockisannetwork.R;
import in.pockisannetwork.communication.presenter.SentMessagesPresenter;
import in.pockisannetwork.communication.presenter.interfaces.ISentMessagesView;
import in.pockisannetwork.ui.adapters.SentMessagesAdapter;
import in.pockisannetwork.ui.models.Message;

/**
 * Created by ashish on 18/4/17.
 */

public class SentMessagesFragment extends Fragment implements ISentMessagesView {
    private Context context;
    @InjectView(R.id.rv_sent_messages)
    RecyclerView rvSentMessages;

    @InjectView(R.id.pb_sent_messages)
    ProgressBar pbSentMessages;

    @InjectView(R.id.tv_sent_no_messages_yet)
    TextView tvYouHaveNotSentAnyMessages;

    private SentMessagesAdapter adapter;
    private SentMessagesPresenter presenter;

    public static Fragment getInstance() {
        SentMessagesFragment sentMessagesFragment = new SentMessagesFragment();
        return sentMessagesFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        context = container.getContext();
        View view = inflater.inflate(R.layout.fragment_sent_messages, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.inject(this, view);
        init();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        presenter = new SentMessagesPresenter();
        presenter.attachView(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        presenter.detachView();
    }

    private void init() {
        tvYouHaveNotSentAnyMessages.setVisibility(View.GONE);
        initAdapter();
        initRecyclerView();
        initData();
    }

    private void initAdapter() {
        adapter = new SentMessagesAdapter();
    }

    private void initData() {
        showProgress();
        presenter.getMessages();
    }

    private void initRecyclerView() {
        rvSentMessages.setLayoutManager(new LinearLayoutManager(context));
        rvSentMessages.setAdapter(adapter);
    }

    @Override
    public void showMessage(String message, int notification_info) {
        ((HomeActivity) context).showMessage(message, notification_info);
    }

    @Override
    public void showMessages(List<Message> messages) {
        hideProgress();
        if (messages.size() == 0) {
            tvYouHaveNotSentAnyMessages.setVisibility(View.VISIBLE);
        } else {
            tvYouHaveNotSentAnyMessages.setVisibility(View.GONE);
        }
        adapter.setMessages(messages);
        adapter.notifyDataSetChanged();
    }

    private void hideProgress() {
        if (pbSentMessages.getVisibility() == View.VISIBLE) {
            pbSentMessages.setVisibility(View.GONE);
        }
    }


    private void showProgress() {
        if (pbSentMessages.getVisibility() == View.GONE) {
            pbSentMessages.setVisibility(View.VISIBLE);
        }
    }
}