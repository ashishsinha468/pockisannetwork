package in.pockisannetwork.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import in.pockisannetwork.ui.activities.HomeActivity;
import in.pockisannetwork.R;
import in.pockisannetwork.communication.model.domain.Contact;
import in.pockisannetwork.communication.presenter.ContactsPresenter;
import in.pockisannetwork.communication.presenter.interfaces.IContactsView;
import in.pockisannetwork.ui.adapters.ContactsAdapter;
import in.pockisannetwork.ui.interfaces.ContactsCallback;

/**
 * Created by ashish on 18/4/17.
 */

public class ContactsFragment extends Fragment implements IContactsView {
    @InjectView(R.id.rv_contacts)
    RecyclerView rvContacts;

    @InjectView(R.id.pb_contacts)
    ProgressBar pbContacts;

    private Context context;
    private ContactsPresenter presenter;
    private ContactsAdapter adapter;

    public static Fragment getInstance() {
        ContactsFragment contactsFragment = new ContactsFragment();
        return contactsFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        presenter = new ContactsPresenter();
        if (presenter.getView() == null) {
            presenter.attachView(this);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contacts, container, false);
        context = container.getContext();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.inject(this, view);
        init();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        presenter.detachView();
    }

    private void init() {
        initAdapter();
        initRecyclerView();
        initData();
    }

    private void initAdapter() {
        adapter = new ContactsAdapter();
        adapter.setCallback(new ContactsCallback() {
            @Override
            public void onContactNameClicked(Contact contact) {
                ((HomeActivity) context).onCustomerContactClicked(contact);
            }
        });
    }

    private void initData() {
        rvContacts.setVisibility(View.VISIBLE);
        showProgress();
        presenter.getContacts();
    }

    private void initRecyclerView() {
        rvContacts.setLayoutManager(new LinearLayoutManager(context));
        rvContacts.setAdapter(adapter);
    }

    @Override
    public void showMessage(String message, int notification_info) {
        hideProgress();
        ((HomeActivity) context).showMessage(message, notification_info);
    }

    void hideProgress() {
        if (pbContacts.getVisibility() == View.VISIBLE) {
            pbContacts.setVisibility(View.GONE);
        }
    }

    void showProgress() {
        if (pbContacts.getVisibility() == View.GONE) {
            pbContacts.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showContacts(List<Contact> contacts) {
        adapter.setContacts(contacts);
        adapter.notifyDataSetChanged();
        hideProgress();
    }
}
