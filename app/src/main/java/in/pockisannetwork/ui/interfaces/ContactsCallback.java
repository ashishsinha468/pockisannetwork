package in.pockisannetwork.ui.interfaces;

import in.pockisannetwork.communication.model.domain.Contact;

/**
 * Created by ashish on 19/4/17.
 */

public interface ContactsCallback {
    void onContactNameClicked(Contact id);
}
