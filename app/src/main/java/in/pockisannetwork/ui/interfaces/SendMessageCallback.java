package in.pockisannetwork.ui.interfaces;

import in.pockisannetwork.ui.models.Message;

/**
 * Created by ashish on 19/4/17.
 */

public interface SendMessageCallback {
    void onYesButtonClicked(Message message);
}
