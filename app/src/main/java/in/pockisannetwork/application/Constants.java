package in.pockisannetwork.application;

/**
 * Created by ashish on 18/4/17.
 */

public class Constants {
    public static final String LOGGER_TAG = "poc_kisan_market";

    public class BUNDLE_KEYS {
        public static final String CONTACT = "contact";
    }
}
