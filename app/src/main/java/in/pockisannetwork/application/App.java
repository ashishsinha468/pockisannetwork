package in.pockisannetwork.application;

import android.app.Application;
import android.content.Context;

import com.squareup.otto.Bus;

import in.pockisannetwork.communication.EventManager;
import in.pockisannetwork.communication.bus.BusProvider;

/**
 * Created by ashish on 18/4/17.
 */

public class App extends Application {
    private static Context context;
    private EventManager mManager;
    private Bus mBus = BusProvider.getInstance();

    public static Context getAppContext() {
        return App.context;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        App.context = getApplicationContext();
        initManagerAndBus();
    }

    private void initManagerAndBus() {
        mManager = new EventManager(this, mBus);
        mBus.register(mManager);
        mBus.register(this);
    }


}

