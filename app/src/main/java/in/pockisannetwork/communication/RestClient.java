package in.pockisannetwork.communication;

import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import in.pockisannetwork.communication.api.ApiConstants;
import in.pockisannetwork.communication.api.interfaces.IUser;
import in.pockisannetwork.communication.model.dtos.ContactsResponseDTO;
import in.pockisannetwork.communication.model.dtos.RestResponse;
import in.pockisannetwork.communication.model.dtos.SmsResponse;
import in.pockisannetwork.utils.GsonUtils;
import in.pockisannetwork.utils.MiscUtils;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

/**
 * Created by ashish on 18/4/17.
 */

class RestClient implements IUser {
    private static final String URL = ApiConstants.BASE_URL;
    private static final String URL_SMS_PROVIDER = "https://api.twilio.com/2010-04-01/Accounts/AC17f4823b5a9e12b20690e7043cf637d9";

    private static RestClient mRestClient;
    private static RestAdapter mRestAdapter;
    private final RestAdapter mRestAdapterForSmsService;
    Gson gson = GsonUtils.getDateCompatibleGson();


    private RestClient() {
        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(ApiConstants.CONNECTION_TIMEOUT, TimeUnit.SECONDS);
        okHttpClient.setConnectTimeout(ApiConstants.CONNECTION_TIMEOUT, TimeUnit.SECONDS);
        //Adding two auth intercepters, one for normal flow through my backend ,and one for the sms provider guys as they need one
        AuthInterceptor authInterceptor = new AuthInterceptor(1);
        okHttpClient.networkInterceptors().add(authInterceptor);

        mRestAdapter = new RestAdapter.Builder()
                .setEndpoint(URL)
                .setClient(new OkClient(okHttpClient))
                .setConverter(new GsonConverter(gson))
                .setLogLevel(MiscUtils.isProduction() ? RestAdapter.LogLevel.NONE : RestAdapter.LogLevel.FULL)
                .build();

        AuthInterceptor authInterceptorSMS = new AuthInterceptor(2);
        okHttpClient.networkInterceptors().add(authInterceptorSMS);
        mRestAdapterForSmsService = new RestAdapter.Builder()
                .setEndpoint(URL_SMS_PROVIDER)
                .setClient(new OkClient(okHttpClient))
                .setConverter(new GsonConverter(gson))
                .setLogLevel(MiscUtils.isProduction() ? RestAdapter.LogLevel.NONE : RestAdapter.LogLevel.FULL)
                .build();
    }

    public static RestClient getClient() {
        if (mRestClient == null)
            mRestClient = new RestClient();
        return mRestClient;
    }

    /**
     * linking of interface to rest-client
     */


    @Override
    public void getContacts(RestCallback<RestResponse<ContactsResponseDTO>> callback) {
        IUser service = mRestAdapter.create(IUser.class);
        service.getContacts(callback);
    }

    @Override
    public void sendMessage(Map<String, String> providerRequestFormat, RestCallback<SmsResponse<Void>> callback) {
        IUser service = mRestAdapterForSmsService.create(IUser.class);
        service.sendMessage(providerRequestFormat, callback);
    }
}

