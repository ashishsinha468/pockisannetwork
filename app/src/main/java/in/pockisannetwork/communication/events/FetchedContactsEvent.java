package in.pockisannetwork.communication.events;

import in.pockisannetwork.communication.model.domain.Contact;

import java.util.List;

/**
 * Created by ashish on 18/4/17.
 */

public class FetchedContactsEvent extends RestResponseEvent {
    List<Contact> contacts;

    public FetchedContactsEvent(List<Contact> contacts, String message, boolean status) {
        super(message, status);
        this.contacts = contacts;
    }

    public List<Contact> getContacts() {
        return contacts;
    }
}
