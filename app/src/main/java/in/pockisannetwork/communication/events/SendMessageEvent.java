package in.pockisannetwork.communication.events;

import java.util.HashMap;
import java.util.Map;

import in.pockisannetwork.communication.model.domain.Contact;
import in.pockisannetwork.communication.model.domain.ProviderRequestFormat;

/**
 * Created by ashish on 19/4/17.
 */

public class SendMessageEvent {
    ProviderRequestFormat providerRequestFormat = new ProviderRequestFormat();

    public SendMessageEvent(Contact contact, String message) {
        providerRequestFormat = new ProviderRequestFormat();
        providerRequestFormat.setMessage(message);
    }

    public Map<String, String> getProviderRequestFormatMap() {
        Map<String, String> fieldMap = new HashMap<>();
        fieldMap.put("To", providerRequestFormat.getDestination());
        fieldMap.put("From", providerRequestFormat.getSendersNumber());
        fieldMap.put("Body", providerRequestFormat.getMessage());
        return fieldMap;
    }
}
