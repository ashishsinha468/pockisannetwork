package in.pockisannetwork.communication.events;

import in.pockisannetwork.communication.model.dtos.ContactsResponseDTO;

/**
 * Created by ashish on 19/4/17.
 */

public class ContactsResponseEvent extends RestResponseEvent{
    ContactsResponseDTO data;
    public ContactsResponseEvent(ContactsResponseDTO data, String message, boolean status) {
        super(message,status);
        this.data=data;
    }

    public ContactsResponseDTO getData() {
        return data;
    }


}
