package in.pockisannetwork.communication.events;

/**
 * Created by ashish on 18/4/17.
 */

public class RestResponseEvent {
    private String message;
    private boolean status;


    public RestResponseEvent(String message, boolean status) {
        this.message = message;
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public boolean isStatus() {
        return status;
    }
}
