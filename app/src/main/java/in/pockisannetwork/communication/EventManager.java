package in.pockisannetwork.communication;

import android.content.Context;

import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import in.pockisannetwork.communication.events.ContactsResponseEvent;
import in.pockisannetwork.communication.events.GetContactsEvent;
import in.pockisannetwork.communication.events.SendMessageEvent;
import in.pockisannetwork.communication.events.SmsResoponseEvent;
import in.pockisannetwork.communication.model.dtos.ContactsResponseDTO;
import in.pockisannetwork.communication.model.dtos.RestResponse;
import in.pockisannetwork.communication.model.dtos.SmsResponse;
import in.pockisannetwork.utils.GsonUtils;
import in.pockisannetwork.utils.MiscUtils;
import in.pockisannetwork.utils.MockUtils;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

import in.pockisannetwork.utils.Logger;

/**
 * Created by ashish on 18/4/17.
 */

public class EventManager {
    private Context mContext;
    private Bus mBus;
    private RestClient mRestClient;

    public EventManager(Context mContext, Bus mBus) {
        this.mContext = mContext;
        this.mBus = mBus;
        this.mRestClient = RestClient.getClient();
    }

    /**
     * listening to events
     */

    @Subscribe
    public void fetchContactsEvent(GetContactsEvent event) {
        RestCallback<RestResponse<ContactsResponseDTO>> callback = new RestCallback<RestResponse<ContactsResponseDTO>>() {

            @Override
            public void success(RestResponse<ContactsResponseDTO> restResponse, Response response) {
                super.success(restResponse, response);
                mBus.post(new ContactsResponseEvent(restResponse.getData(), restResponse.getMessage(), restResponse.isStatus()));
            }

            @Override
            public void failure(RetrofitError error) {

//                mBus.post(new ContactsResponseEvent(null, getCompleteErrorMessage(error), false));
//                MOCK //TODO remember to switch it off when not in use.
                mBus.post(new ContactsResponseEvent(MockUtils.getContacts(), "Mocked Data for now", true));
            }
        };
        mRestClient.getContacts(callback);
    }


    @Subscribe
    public void sendSms(SendMessageEvent event) {
        RestCallback<SmsResponse<Void>> callback = new RestCallback<SmsResponse<Void>>() {

            @Override
            public void success(SmsResponse<Void> smsRsponse, Response response) {
                super.success(smsRsponse, response);
                mBus.post(new SmsResoponseEvent(smsRsponse.getMessage(), true));
            }

            @Override
            public void failure(RetrofitError error) {
                mBus.post(new SmsResoponseEvent(getCompleteErrorMessage(error), false));
            }
        };
        mRestClient.sendMessage(event.getProviderRequestFormatMap(), callback);
    }


    /**
     * Helper method to parse REST contract response in case of error;
     */

    private String getCompleteErrorMessage(RetrofitError error) {
        //TODO for now called here but this is shifted to super of callback should be called as super.failure in callback anonymous
        Logger.logError("something happened in an api call..." + error.getUrl() + "\nerror details " + error.getMessage());
        return getExtraMessage(error) + getErrorMessage(error); //TODO refactor this
    }

    private String getExtraMessage(RetrofitError error) {
        String extraMessage = "";
        try {

            String responseBody = new String(((TypedByteArray) error.getResponse().getBody()).getBytes()).toString();
            RestResponse<String> response = GsonUtils.getObjectFromJson(responseBody, RestResponse.class);
            extraMessage = "" + response.getMessage() + getErrorMessage(error);
        } catch (Exception e) {
            // you know when non server related thing happens.
            Logger.logError("Aomething happened in parsing error response from server extraMessage");
            e.printStackTrace();
        }
        return " " + extraMessage;
    }

    private String getErrorMessage(RetrofitError error) {
        if (!MiscUtils.isProduction()) {
            return error.getMessage();
        }
        return "";
    }

}

