package in.pockisannetwork.communication.model.dtos;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ashish on 18/4/17.
 */

public class RestResponse<T> {

    @SerializedName("responseData")
    private T data;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private boolean status;

    public String getMessage() {
        return message;
    }

    public boolean isStatus() {
        return status;
    }

    public T getData() {
        return data;
    }
}
