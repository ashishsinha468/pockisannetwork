package in.pockisannetwork.communication.model.dtos;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ashish on 19/4/17.
 */

public class SmsResponse<T> {

    @SerializedName("status")
    private String message;

    @SerializedName("sid")
    private String sid;

    //TODO lets see if we need any other parameters


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }
}
