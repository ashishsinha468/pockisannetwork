package in.pockisannetwork.communication.model.dtos;

import com.google.gson.annotations.SerializedName;

import in.pockisannetwork.communication.model.domain.Contact;

import java.util.List;

/**
 * Created by ashish on 19/4/17.
 */

public class ContactsResponseDTO {
    @SerializedName("contacts")
    private List<Contact> contacts;

    public List<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }
}
