package in.pockisannetwork.communication.model.domain;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ashish on 19/4/17.
 */

public class ProviderRequestFormat {
    @SerializedName("To")
    private String destination = "+917972570352";//trial account have to hard code these for now

    @SerializedName("From")
    private String sendersNumber = "+14154187544";

    @SerializedName("Body")
    private String message;

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public void setSendersNumber(String sendersNumber) {
        this.sendersNumber = sendersNumber;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDestination() {
        return destination;
    }

    public String getSendersNumber() {
        return sendersNumber;
    }

    public String getMessage() {
        return message;
    }
}
