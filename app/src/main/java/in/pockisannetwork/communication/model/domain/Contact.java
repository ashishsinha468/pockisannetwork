package in.pockisannetwork.communication.model.domain;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ashish on 18/4/17.
 */

public class Contact {
    @SerializedName("id")
    private String id;

    @SerializedName("firstName")
    private String firstName;

    @SerializedName("lastName")
    private String lastName;

    @SerializedName("phoneNumber")
    private String phoneNumber;//Assuming just one number per contact..could have used a list for multiple numbers..also assuming the number is from india as of now..not considering country code


    //Setters were not needed but have to mock KEY_DATA to setters might and will be used
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
