package in.pockisannetwork.communication.api;

/**
 * Created by ashish on 18/4/17.
 */

public class ApiConstants {
    public static final String PROD_URL = "https://www.google.com";
    public static final long CONNECTION_TIMEOUT = 10l;
    public static final String BASE_URL = "http://www.google.com";
    public static String USED_URL = BASE_URL;

    public class REST_API {
        public static final String AUTH_HEADER_KEY = "x-authorization-token";
        public static final String GET_CONTACTS = "/getContacts/";
        public static final String SEND_SMS = "/Messages.json";
    }
}
