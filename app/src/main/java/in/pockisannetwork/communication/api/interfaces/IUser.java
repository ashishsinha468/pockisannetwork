package in.pockisannetwork.communication.api.interfaces;

import java.util.Map;

import in.pockisannetwork.communication.RestCallback;
import in.pockisannetwork.communication.api.ApiConstants;
import in.pockisannetwork.communication.events.SendMessageEvent;
import in.pockisannetwork.communication.model.domain.ProviderRequestFormat;
import in.pockisannetwork.communication.model.dtos.ContactsResponseDTO;
import in.pockisannetwork.communication.model.dtos.RestResponse;
import in.pockisannetwork.communication.model.dtos.SmsResponse;
import retrofit.http.Body;
import retrofit.http.FieldMap;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;

/**
 * Created by ashish on 19/4/17.
 */

public interface IUser {

    @GET(ApiConstants.REST_API.GET_CONTACTS)
    void getContacts(RestCallback<RestResponse<ContactsResponseDTO>> callback);

    @FormUrlEncoded
    @POST(ApiConstants.REST_API.SEND_SMS)
    void sendMessage(@FieldMap Map<String, String> providerRequestFormat, RestCallback<SmsResponse<Void>> callback);
}
