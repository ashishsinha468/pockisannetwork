package in.pockisannetwork.communication.presenter;

import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import in.pockisannetwork.R;
import in.pockisannetwork.communication.bus.BusProvider;
import in.pockisannetwork.communication.events.ContactsResponseEvent;
import in.pockisannetwork.communication.events.GetContactsEvent;
import in.pockisannetwork.communication.presenter.interfaces.BasePresenter;
import in.pockisannetwork.communication.presenter.interfaces.IContactsView;
import in.pockisannetwork.utils.MiscUtils;

/**
 * Created by ashish on 18/4/17.
 */

public class ContactsPresenter implements BasePresenter<IContactsView> {
    Bus mBus = BusProvider.getInstance();
    IContactsView view = null;

    @Override
    public void attachView(IContactsView view) {
        this.view = view;
        mBus.register(this);
    }

    @Override
    public void detachView() {
        this.view = null;
        mBus.unregister(this);

    }


    public IContactsView getView() {
        return view;
    }

    public void getContacts() {
    //Not checking internet connection over here..anyways have to mock data for now..when call fails mocked data fails, user need not retry
        mBus.post(new GetContactsEvent());

    }

    @Subscribe
    public void onFetchedContactsEvent(ContactsResponseEvent event) {

        if (event.isStatus()) {
            view.showContacts(event.getData().getContacts());
        } else {
            view.showMessage(event.getMessage(), R.color.notification_error);
        }
    }
}
