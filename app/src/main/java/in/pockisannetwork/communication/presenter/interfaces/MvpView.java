package in.pockisannetwork.communication.presenter.interfaces;

import android.content.Context;

/**
 * Created by ashish on 18/4/17.
 */

public interface MvpView {
    Context getContext();

    void showMessage(String string, int notification_info);
}
