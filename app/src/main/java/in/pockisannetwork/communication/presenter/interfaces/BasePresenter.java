package in.pockisannetwork.communication.presenter.interfaces;

/**
 * Created by ashish on 18/4/17.
 */

public interface BasePresenter<V> {
    void attachView(V view);

    void detachView();
}
