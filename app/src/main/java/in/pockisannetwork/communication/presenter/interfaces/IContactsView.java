package in.pockisannetwork.communication.presenter.interfaces;

import java.util.List;

import in.pockisannetwork.communication.model.domain.Contact;

/**
 * Created by ashish on 18/4/17.
 */

public interface IContactsView extends MvpView {
    void showContacts(List<Contact> contacts);
}
