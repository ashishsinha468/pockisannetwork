package in.pockisannetwork.communication.presenter.interfaces;

import in.pockisannetwork.ui.models.Message;

import java.util.List;

/**
 * Created by ashish on 19/4/17.
 */

public interface ISentMessagesView extends MvpView {
    void showMessages(List<Message> messages);
}
