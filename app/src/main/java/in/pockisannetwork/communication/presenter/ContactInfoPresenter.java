package in.pockisannetwork.communication.presenter;

import android.app.ProgressDialog;

import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import in.pockisannetwork.R;
import in.pockisannetwork.application.App;
import in.pockisannetwork.communication.bus.BusProvider;
import in.pockisannetwork.communication.events.SendMessageEvent;
import in.pockisannetwork.communication.events.SmsResoponseEvent;
import in.pockisannetwork.communication.model.domain.Contact;
import in.pockisannetwork.communication.presenter.interfaces.BasePresenter;
import in.pockisannetwork.communication.presenter.interfaces.IContactInfoView;
import in.pockisannetwork.db.DbCRUDException;
import in.pockisannetwork.db.DbHandler;
import in.pockisannetwork.ui.models.Message;
import in.pockisannetwork.utils.DateUtils;
import in.pockisannetwork.utils.MiscUtils;

/**
 * Created by ashish on 19/4/17.
 */

public class ContactInfoPresenter implements BasePresenter<IContactInfoView> {
    Bus mBus = BusProvider.getInstance();
    IContactInfoView view = null;
    private Message cachedMessage;

    @Override
    public void attachView(IContactInfoView view) {
        this.view = view;
        mBus.register(this);
    }

    @Override
    public void detachView() {
        mBus.unregister(this);
        this.view = null;
    }

    public IContactInfoView getView() {
        return view;
    }

    public void sendMessageTo(Contact contact, Message message) {
        if (MiscUtils.isConnectedToInternet()) {
            mBus.post(new SendMessageEvent(contact, message.toString()));
            message.setContactName(contact.getFirstName() + " " + contact.getLastName());
            message.setTimestamp(DateUtils.getCurrentTimestamp());
            cachedMessage = message;
        } else {
            view.showMessage(view.getContext().getString(R.string.label_no_internet_connection), R.color.notification_error);
        }
    }

    @Subscribe
    public void onMessageSentEvent(SmsResoponseEvent event) {
        if (event.isStatus()) {
            view.showMessage(view.getContext().getString(R.string.label_message_sent_succesfully), R.color.notification_success);
            saveContactToDb(cachedMessage);
        } else {
            view.showMessage(event.getMessage(), R.color.notification_error);
        }
    }

    private void saveContactToDb(Message cachedMessage) {
        try {
            DbHandler instance = DbHandler.getInstance(App.getAppContext());
            instance.saveMessageLocally(cachedMessage);
        } catch (DbCRUDException e) {
            e.printStackTrace();
        }
    }
}
