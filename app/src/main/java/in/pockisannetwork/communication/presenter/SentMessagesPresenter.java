package in.pockisannetwork.communication.presenter;

import java.util.List;

import in.pockisannetwork.R;
import in.pockisannetwork.application.App;
import in.pockisannetwork.communication.presenter.interfaces.BasePresenter;
import in.pockisannetwork.communication.presenter.interfaces.ISentMessagesView;
import in.pockisannetwork.db.DbCRUDException;
import in.pockisannetwork.db.DbHandler;
import in.pockisannetwork.ui.models.Message;
import in.pockisannetwork.utils.Logger;

/**
 * Created by ashish on 19/4/17.
 */

public class SentMessagesPresenter implements BasePresenter<ISentMessagesView> {
    ISentMessagesView view;

    //No need of bus instance over here..not making any network calls for now
    @Override
    public void attachView(ISentMessagesView view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

    public void getMessages() {
        try {
            DbHandler instance = DbHandler.getInstance(App.getAppContext());
            List<Message> messages = instance.getMessages();
            view.showMessages(messages);
        } catch (DbCRUDException e) {
            Logger.logError(e.getMessage());
            view.showMessage(e.getMessage(), R.color.notification_error);
        }
    }
}
