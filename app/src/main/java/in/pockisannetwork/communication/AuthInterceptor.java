package in.pockisannetwork.communication;

import android.util.Base64;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

import in.pockisannetwork.communication.api.ApiConstants;
import in.pockisannetwork.utils.Logger;

/**
 * Created by ashish on 19/4/17.
 */

class AuthInterceptor implements Interceptor {
    int TYPE_NORMAL = 1;
    int TYPE_SMS_PROVIDER = 2;
    int type = TYPE_NORMAL;

    public AuthInterceptor(int type) {
        this.type = type;
    }

    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Request originalRequest = chain.request();
        String username = "AC17f4823b5a9e12b20690e7043cf637d9";
        String password = "f712a05bd4c3fdeede3b24d3f0b12ff7";

        //TODO adding username password in header for all requests..TODO, have to have filter over here
        String credentials = username + ":" + password;

        String authToken = "some auth token";//TODO harcoding for now..change it;
        final String basic =
                "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
        // Add authorization header with updated authorization value to intercepted request
        Request authorisedRequest;
        if (type == TYPE_SMS_PROVIDER) {
            authorisedRequest = originalRequest.newBuilder()
                    .header(ApiConstants.REST_API.AUTH_HEADER_KEY, authToken)
                    .header("Authorization", basic)
                    .build();
        } else {
            authorisedRequest = originalRequest.newBuilder()
                    .header(ApiConstants.REST_API.AUTH_HEADER_KEY, authToken)
                    .build();
        }
        Logger.logError("Sending request..." + authorisedRequest.toString());
        return chain.proceed(authorisedRequest);
    }
}

