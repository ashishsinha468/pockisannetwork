package in.pockisannetwork.communication;

import in.pockisannetwork.utils.Logger;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ashish on 19/4/17.
 */

public class RestCallback<T> implements Callback<T> {
    @Override
    public void success(T t, Response response) {
        Logger.logError("Succesfull response from api " + response.getUrl());
        Logger.logError("response body" + response.toString());
    }

    @Override
    public void failure(RetrofitError error) {
        Logger.logError("something happened in an api call..." + error.getUrl() + "\nerror details " + error.getMessage());
    }
}