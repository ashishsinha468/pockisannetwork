package in.pockisannetwork.utils;

import android.app.Dialog;
import android.content.Context;
import android.text.format.*;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import in.pockisannetwork.R;
import in.pockisannetwork.ui.interfaces.SendMessageCallback;
import in.pockisannetwork.ui.models.Message;

/**
 * Created by ashish on 19/4/17.
 */

public class DialogUtils {
    public static void showSendMessageScreen(final SendMessageCallback sendMessageCallback, Context context) {
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_send_message);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        TextView tvOtpMessage = (TextView) dialog.findViewById(R.id.tv_send_otp_message);
        final Message message = new Message();
        message.setText(context.getString(R.string.label_hi_your_otp_is));
        message.setOtp(getOtp());
        tvOtpMessage.setText(message.toString());

        Button sendButton = (Button) dialog.findViewById(R.id.btn_send);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                sendMessageCallback.onYesButtonClicked(message);
            }
        });

        Button cancelButton = (Button) dialog.findViewById(R.id.btn_cancel);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });


        dialog.getWindow().setAttributes(lp);
        dialog.show();
    }

    private static String getOtp() {
        //This is my otp logic for now
        String currentTimestamp = DateUtils.getCurrentTimestamp();
        Long millis = Long.valueOf(currentTimestamp);
        return String.valueOf(millis % 123456);
    }
}
