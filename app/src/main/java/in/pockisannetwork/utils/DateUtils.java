package in.pockisannetwork.utils;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.Minutes;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import in.pockisannetwork.R;
import in.pockisannetwork.application.App;

/**
 * Created by ashish on 19/4/17.
 */

public class DateUtils {
    public static String getDateAndTimeFromTimestampString(String str) {
        Long timestamp = Long.parseLong(str);
        Date date = new Date(timestamp);
        DateFormat format = new SimpleDateFormat(App.getAppContext().getString(R.string.date_format_human));
        String formatted = format.format(date);
        return formatted;
    }

    public static String getCurrentTimestamp() {
        long millis = new Date().getTime();
        return String.valueOf(millis);
    }
}


