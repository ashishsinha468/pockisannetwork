package in.pockisannetwork.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import in.pockisannetwork.application.App;
import in.pockisannetwork.communication.api.ApiConstants;

/**
 * Created by ashish on 18/4/17.
 */

public class MiscUtils {

    public static boolean isConnectedToInternet() {
        Context context = App.getAppContext();
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public static boolean isProduction() {
        if ((ApiConstants.USED_URL).equalsIgnoreCase(ApiConstants.PROD_URL))
            return true;

        return false;
    }
}

