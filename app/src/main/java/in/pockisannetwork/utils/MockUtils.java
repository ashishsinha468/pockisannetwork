package in.pockisannetwork.utils;

import java.util.ArrayList;
import java.util.List;

import in.pockisannetwork.communication.model.domain.Contact;
import in.pockisannetwork.communication.model.dtos.ContactsResponseDTO;

/**
 * Created by ashish on 19/4/17.
 */

public class MockUtils {

    public static ContactsResponseDTO getContacts() {
        ContactsResponseDTO contactsResponseDTO = new ContactsResponseDTO();
        List<Contact> contacts = new ArrayList<>();
        Contact contact1 = new Contact();
        contact1.setFirstName("Contact");
        contact1.setLastName("1");
        contact1.setPhoneNumber("8983299529");
        Contact contact2 = new Contact();
        contact2.setFirstName("Contact");
        contact2.setLastName("2");
        contact2.setPhoneNumber("8983299530");
        Contact contact3 = new Contact();
        contact3.setFirstName("Contact");
        contact3.setLastName("3");
        contact3.setPhoneNumber("8983299531");
        Contact contact4 = new Contact();
        contact4.setFirstName("Contact");
        contact4.setLastName("4");
        contact4.setPhoneNumber("8983299532");
        Contact contact5 = new Contact();
        contact5.setFirstName("Contact");
        contact5.setLastName("5");
        contact5.setPhoneNumber("8983299533");
        Contact contact6 = new Contact();
        contact6.setFirstName("Contact");
        contact6.setLastName("6");
        contact6.setPhoneNumber("8983299534");

        contacts.add(contact1);
        contacts.add(contact2);
        contacts.add(contact3);
        contacts.add(contact4);
        contacts.add(contact5);
        contacts.add(contact6);
        contactsResponseDTO.setContacts(contacts);
        return contactsResponseDTO;
    }
}
