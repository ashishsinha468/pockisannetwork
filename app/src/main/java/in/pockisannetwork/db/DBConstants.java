package in.pockisannetwork.db;

/**
 * Created by ashish on 19/4/17.
 */
public class DBConstants {
    public static final String DB_NAME = "pockisannetwork";
    public static final int DB_VERSION = 2;

    public static class SENT_MESSAGES {
        public static final String KEY_DATA = "message";
        public static String TABLE_NAME = "SentMessages";
        public static String KEY_ID = "id";
    }

    public static class CREATE_QUERIES {
        public static final String CREATE_TABLE_SENT_MESSAGES = "CREATE TABLE " + SENT_MESSAGES.TABLE_NAME + " ( " + SENT_MESSAGES.KEY_ID + " INTEGER PRIMARY KEY, " + SENT_MESSAGES.KEY_DATA + " TEXT )";
    }

    public static class DROP_QUERIES {
        public static final String SENT_MESSAGES = "DROP TABLE IF EXISTS " + DBConstants.SENT_MESSAGES.TABLE_NAME;
    }
}
