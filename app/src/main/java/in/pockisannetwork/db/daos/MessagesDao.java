package in.pockisannetwork.db.daos;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import in.pockisannetwork.application.Constants;
import in.pockisannetwork.communication.model.domain.Contact;
import in.pockisannetwork.db.DBConstants;
import in.pockisannetwork.db.DbCRUDException;
import in.pockisannetwork.db.interfaces.MessagesActions;
import in.pockisannetwork.ui.models.Message;
import in.pockisannetwork.utils.GsonUtils;

/**
 * Created by ashish on 19/4/17.
 */

public class MessagesDao implements MessagesActions {
    private String TABLE_NAME;
    private SQLiteDatabase db;

    public MessagesDao(SQLiteDatabase writableDatabase) {
        this.db = writableDatabase;
        this.TABLE_NAME = DBConstants.SENT_MESSAGES.TABLE_NAME;
    }

    @Override
    public void addMessage(Message message) throws DbCRUDException {
        insertMessageInToDatabase(message);
    }

    private void insertMessageInToDatabase(Message message) throws DbCRUDException {
        try {
            ContentValues values = new ContentValues();
            values.put(DBConstants.SENT_MESSAGES.KEY_ID, message.getTimestamp());
            values.put(DBConstants.SENT_MESSAGES.KEY_DATA, GsonUtils.getJsonString(message));
            db.insert(TABLE_NAME, null, values);
//            db.close();
        } catch (Exception e) {
            throw new DbCRUDException(e);
        }
    }


    @Override
    public List<Message> getMessages() throws DbCRUDException {
        List<Message> messages;
        try {
            messages = new ArrayList<>();
            String QUERY = "SELECT * FROM " + TABLE_NAME + " ORDER BY " + DBConstants.SENT_MESSAGES.KEY_ID + " DESC";
            Cursor cursor = db.rawQuery(QUERY, null);
            if (!cursor.isLast()) {
                while (cursor.moveToNext()) {
                    Message message = GsonUtils.getObjectFromJson(cursor.getString(1), Message.class);
                    messages.add(message);
                }
            }
            cursor.close();
//            db.close();
        } catch (Exception e) {
            throw new DbCRUDException(e);

        }
        return messages;
    }

    @Override
    public void clearTable() throws DbCRUDException {
        String query = "delete from " + TABLE_NAME;
        try {
            db.execSQL(query);
        } catch (Exception e) {
            throw new DbCRUDException(e);
        }
    }
}
