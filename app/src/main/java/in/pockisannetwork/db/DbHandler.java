package in.pockisannetwork.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.List;

import in.pockisannetwork.db.daos.MessagesDao;
import in.pockisannetwork.ui.models.Message;

/**
 * Created by ashish on 19/4/17.
 */

public class DbHandler extends SQLiteOpenHelper {

    private static DbHandler instance;

    private DbHandler(Context context) {
        super(context, DBConstants.DB_NAME, null, DBConstants.DB_VERSION);
    }

    public static synchronized DbHandler getInstance(Context context) {
        if (instance == null) {
            instance = new DbHandler(context);
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DBConstants.CREATE_QUERIES.CREATE_TABLE_SENT_MESSAGES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DBConstants.DROP_QUERIES.SENT_MESSAGES);
        onCreate(db);
    }

    public void dropAllTables() {
        try {
            clearTable(DBConstants.SENT_MESSAGES.TABLE_NAME);
        } catch (DbCRUDException e) {
            e.printStackTrace();
        }
    }


    /**
     * CRUDs
     */

    public void clearTable(String tableName) throws DbCRUDException {
        new MessagesDao(this.getWritableDatabase()).clearTable();
    }


    public void saveMessageLocally(Message message) throws DbCRUDException {
        new MessagesDao(this.getWritableDatabase()).addMessage(message);
    }

    public List<Message> getMessages() throws DbCRUDException {
        return new MessagesDao(this.getWritableDatabase()).getMessages();
    }
}

