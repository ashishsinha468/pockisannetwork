package in.pockisannetwork.db.interfaces;

import in.pockisannetwork.communication.model.domain.Contact;
import in.pockisannetwork.db.DbCRUDException;
import in.pockisannetwork.ui.models.Message;

import java.util.List;

/**
 * Created by ashish on 19/4/17.
 */

public interface MessagesActions {
    void addMessage(Message contact) throws DbCRUDException;

    List<Message> getMessages() throws DbCRUDException;

    void clearTable() throws DbCRUDException;
}
