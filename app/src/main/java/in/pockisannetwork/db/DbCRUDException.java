package in.pockisannetwork.db;

/**
 * Created by ashish on 19/4/17.
 */

public class DbCRUDException extends Throwable {
    private final Exception exception;

    public DbCRUDException(Exception e) {
        this.exception = e;
    }

    @Override
    public void printStackTrace() {
        exception.printStackTrace();
    }

    @Override
    public String getMessage() {
        return exception.getMessage();
    }
}

